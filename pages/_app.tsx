import '@styles/globals.css'
import type { AppProps } from 'next/app'
import { NextPage } from 'next'
import { ReactElement, ReactNode, useState } from 'react'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  layout: (page: ReactElement) => ReactNode
}

export type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

/**
 * # Terabio App
 *
 * the app to manage all of the data
 * and child for pages
 *
 * @returns JSX.Element
 */
const TerabioApp = ({
  Component,
  pageProps,
}: AppPropsWithLayout): JSX.Element => {
  const [queryClient] = useState(() => new QueryClient())

  // define the layout if exists
  const layout = Component.layout ?? ((page) => page)

  return (
    <QueryClientProvider client={queryClient}>
      {layout(<Component {...pageProps} />)}
    </QueryClientProvider>
  )
}

export default TerabioApp
