const forms = require('@tailwindcss/forms')
const lineClamp = require('@tailwindcss/line-clamp')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    fontFamily: {
      dm: ['DM Sans', 'sans-serif'],
      poppins: ['Poppins', 'sans-serif'],
    },
    screens: {
      tablet: '640px',
      laptop: '1024px',
      desktop: '1280px',
    },
    extend: {
      colors: {
        'green-lizard': '#9EFB27',
      },
    },
  },
  plugins: [forms, lineClamp],
}
